/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import phanmot.entity.Account;
import phanmot.entity.Book;
import phanmot.entity.BookCopy;
import phanmot.entity.BookRegistrationHistory;

/**
 *
 * @author username
 */
public class BookRegistrationController {

    /**
     * Kiểm tra điều kiện mượn sách và tiến hành mượn sách..
     *
     * @param id xác định tài khoản mượn sách.
     * @param book xác định sách muốn mượn.
     * @param number xác định số lượng sách định mượn.
     * @return Trả về<li>0 nếu đăng kí thành công.
     * <li>1 nếu mượn quá số lượng.
     * <li>2 nếu số bản copy đã hết.
     * <li>3 nếu thẻ mượn sách hết hạn.
     * <li>4 nếu chưa đăng nhập
     * @throws java.sql.SQLException
     * @throws java.text.ParseException
     */
    public int checkRegistration(int id, Book book, int number) throws SQLException, ParseException {
        //Kiem tra
        Account account = new Account();
        if (!account.checkID(id)) {
            return 4; // chua dang nhap
        } else {
            if (account.getLoaningCard().cardCheck()) {
                BookCopy copy = new BookCopy();
                if (copy.checkBookQuantity(book.getId())) {
                    BookRegistrationHistory history = new BookRegistrationHistory(account, copy);
                    if (history.check(number)) {
                        bookRegister(history);
                        return 0; // thanh cong
                    } else {
                        return 1; // muon qua so luong
                    }
                } else {
                    return 2; //het sach;                          
                }
            }
            return 3;//the het han
        }

    }

    /**
     * Thực hiện việc đăng kí mượn sách.
     *
     * @param history tham số xác đinh việc xác định mượn sách
     * @throws java.sql.SQLException
     */
    private void bookRegister(BookRegistrationHistory history) throws SQLException, ParseException {
        history.addBookRegisTrationHistory();
        history.getBookCopy().updateState("borrowed");
    }
}
