/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import phanmot.database.DBConnection;

/**
 *
 * @author username
 */
public class LoaningCard {

    private int cardNumber;
    private Date dateBegin;
    private Date dateEnd;

    private static final String FIND_CARD = "select * from loaningcard where id_card = ?";

    /**
     * Khởi tạo đối tượng LoaningCard với đầy đủ tham số
     *
     * @param cardNumber đối số xác định thuộc tính cardNumber.
     * @param dateBegin đối số xác đinh thuộc tính dayBegin.
     * @param dateEnd đối số xác định thuộc tính dayEnd
     */
    public LoaningCard(int cardNumber, Date dateBegin, Date dateEnd) {
        this.cardNumber = cardNumber;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    /**
     * Khơi tạo đối tượng LoaningCard
     */
    public LoaningCard() {

    }

    /**
     * Đặt giá trị cho thuộc tính cardNumber của đối tương LoaningCard
     *
     * @param cardNumber đối số xác định giá trị của cardNumber
     */
    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * Trả về giá trị của thuộc tính cardNumber của đối tượng LoaningCard
     *
     * @return giá trị của thuộc tính cardNumber
     */
    public int getCardNumber() {
        return this.cardNumber;
    }

    /**
     * Đặt giá trị cho thuộc tính dateBegin của đối tương LoaningCard
     *
     * @param dateBegin đối số xác định giá trị của dateBegin
     */
    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    /**
     * Trả về giá trị của thuộc tính dateBegin của đối tượng LoaningCard
     *
     * @return giá trị của thuộc tính dateBegin
     */
    public Date getDateBegin() {
        return this.dateBegin;
    }

    /**
     * Đặt giá trị cho thuộc tính dateEnd của đối tương LoaningCard
     *
     * @param cdateEnd đối số xác định giá trị của của dateEnd
     */
    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * Trả về giá trị của thuộc tính dateEnd của đối tượng LoaningCard
     *
     * @return giá trị của thuộc tính dateEnd
     */
    public Date getDateEnd() {
        return this.dateEnd;
    }

    /**
     * Tìm Loaning Card qua id
     *
     * @param id đối số xác định id của thẻ
     * @return đối tượng LoaningCard có id giống id truyền vào
     * @throws SQLException
     */
    public static LoaningCard findCard(int id) throws SQLException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(FIND_CARD);
        ptmt.setInt(1, id);
        ResultSet rs = ptmt.executeQuery();
        LoaningCard card = new LoaningCard();
        if (rs.next()) {
            card.setCardNumber(rs.getInt("id_card"));
            card.setDateBegin(rs.getDate("day_begin"));
            card.setDateEnd(rs.getDate("day_end"));
            connection.close();
            return card;
        }
        connection.close();
        return null;
    }

    /**
     * So sách hai đối tượng LoaningCard
     *
     * @param obj đối số xác đinh đối tượng LoaningCard cần so sánh.
     * @return Trả về true nếu hai đối tượng giống nhau. Trả về false nếu hai
     * đối tượng khác nhau
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LoaningCard) {
            LoaningCard card = (LoaningCard) obj;
            return this.getCardNumber() == card.getCardNumber()
                    && this.getDateBegin().equals(card.getDateBegin())
                    && this.getDateEnd().equals(card.getDateEnd());
        }
        return false;
    }

    /**
     * Kiểm tra đối tượng card còn hoạt động được hay không.
     * <li>Trả về <b>True</b> nếu còn hạn.
     * <li>Trả về <b>False</b> nếu đã hết hạn.
     *
     * @return Trả về <b>True</b> nếu đối tượng LoaningCard còn hoạt động được,
     * trả về
     * <b>False</b> nếu không.
     */
    public boolean cardCheck() {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();

        Date date1 = this.getDateEnd();
        Date date2 = new Date();

        c1.setTime(date1);
        c2.setTime(date2);

        long noDay = (c2.getTime().getTime() - c1.getTime().getTime())
                / (24 * 3600 * 1000);
        if (noDay <= 0) {
            return true;
        }
        return false;
    }

}
