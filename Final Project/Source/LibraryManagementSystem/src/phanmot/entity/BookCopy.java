/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import phanmot.database.DBConnection;

/**
 *
 * @author username
 */
public class BookCopy {

    private Book book;
    private int index;
    private String state;

    private static final String COUNT_BOOK = "select count(bookindex) from lms.bookcopy where id_book = ? and state = 'available'";
    private static final String FIND_BOOK = "select * from lms.bookcopy where id_book = ? and bookindex>= all(select bookindex from bookcopy where id_book = ? and state = 'available') ;";
    private static final String FIND_BOOK_COPY = "select * from lms.bookcopy where bookindex = ?";
    private final String UPDATE = "update bookcopy set state = ? where bookindex = ?";

    /**
     * Khởi tạo đối tượng BookCopy với đầy đủ tham số.
     *
     * @param book đối số xác định thuộc tính book.
     * @param index đối số xác định thuộc tính index.
     * @param state đối số xác định thuộc tính state.
     */
    public BookCopy(Book book, int index, String state) {
        this.book = book;
        this.index = index;
        this.state = state;
    }

    /**
     * Khởi tạo đối tượng BookCopy
     */
    public BookCopy() {

    }

    /**
     * Đặt giá trị cho thuộc tính book của đối tượng BookCop.
     *
     * @param book đối số xác định giá trị của tham số book,.
     */
    public void setBook(Book book) {
        this.book = book;
    }

    /**
     * Trả về giá trị của thuộc tính book của đối tượng BookCopy.
     *
     * @return giá trị của thuộc tính book.
     */
    public Book getBook() {
        return this.book;
    }

    /**
     * Đặt giá trị cho thuộc tính index của đối tượng BookCopy.
     *
     * @param index đối số xác định giá trị của tham số index.
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * Trả về giá trị của thuộc tính index của đối tượng BookCopy.
     *
     * @return giá trị của thuộc tính index.
     */
    public int getIndex() {
        return this.index;
    }

    /**
     * Đặt giá trị cho thuộc tính state của đối tượng BookCopy.
     *
     * @param state đối số xác định giá trị của tham số state.
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Trả về giá trị của thuộc tính state của đối tượng BookCopy.
     *
     * @return giá trị của thuộc tính state.
     */
    public String getState() {
        return this.state;
    }

    /**
     * Cập nhật trạng thái của đối tượng BookCopy
     *
     * @param str đối số xác định trạng thái
     * @return <li>true nếu thành công</li>
     * <li>false nếu thất bại</li>
     * @throws SQLException
     */
    public boolean updateState(String str) throws SQLException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(UPDATE);
        ptmt.setString(1, str);
        ptmt.setInt(2, this.index);
        if (ptmt.executeUpdate() == 0) {
            return false;
        }
        return true;
    }

    /**
     * Kiểm tra số sách còn lại trong kho.
     *
     * @param id đối số xác định id của sách.
     * @return <li>true nếu còn sách</li>
     * <li>false nếu hết sách</li>
     * @throws SQLException
     */
    public boolean checkBookQuantity(int id) throws SQLException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(COUNT_BOOK);
        ptmt.setInt(1, id);
        ResultSet rs = ptmt.executeQuery();
        if (rs.next()) {
            int number = rs.getInt("count(bookindex)");
            if (number > 0) {
                findBook(id);
                connection.close();
                return true;
            }
            connection.close();
            return false;
        }
        connection.close();
        return false;
    }
// cho dang kí

    /**
     * Tìm một copy của sách
     *
     * @param id xác định id sách
     * @return <li>true nếu tìm thấy copy</li>
     * <li>false nếu không tìm thấy</li>
     * @throws SQLException
     */
    public boolean findBook(int id) throws SQLException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(FIND_BOOK);
        ptmt.setInt(1, id);
        ptmt.setInt(2, id);
        ResultSet rs = ptmt.executeQuery();
        if (rs.next()) {
            this.setBook(Book.findByID(id));
            this.setIndex(rs.getInt("bookindex"));
            this.setState(rs.getString("state"));
            connection.close();
            return true;
        }
        connection.close();
        return false;

    }
//cho kiem tra

    /**
     * Tìm một copy của sách
     *
     * @param index xác định index của copy
     * @return đối tượng BookCopy có index giống index truyền vào
     * @throws SQLException
     */
    public static BookCopy findBookCopy(int index) throws SQLException {
        BookCopy copy = new BookCopy();
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(FIND_BOOK_COPY);
        ptmt.setInt(1, index);
        ResultSet rs = ptmt.executeQuery();
        if (rs.next()) {
            copy.setBook(Book.findByID(rs.getInt("id_book")));
            copy.setIndex(rs.getInt("bookindex"));
            copy.setState(rs.getString("state"));
            connection.close();
            return copy;
        }
        connection.close();
        return null;
    }

    /**
     * So sánh hai đối tượng Bookcopy
     *
     * @param obj đối số xác định đối tượng BookCopy cần so sánh.
     * @return <li>true nếu hai đối tượng giống nhau</li>
     * <li>false nếu hai đối tượng là khác nhau</li>
     */
    public boolean equals(Object obj) {
        if (obj instanceof BookCopy) {
            BookCopy copy = (BookCopy) obj;
            return this.getBook().equals(copy.getBook())
                    && this.getIndex() == copy.getIndex()
                    && this.getState().equals(copy.getState());
        }
        return false;
    }

}
