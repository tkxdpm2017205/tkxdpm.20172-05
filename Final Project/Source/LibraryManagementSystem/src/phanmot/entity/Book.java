/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;
import phanmot.database.DBConnection;

/**
 *
 * @author username
 */
public class Book {

    private int id;
    private String title;
    private String publishingCompany;
    private String author;
    private String classification;

    private final String SEARCH_BY_TITLE = "select * from lms.book where title = ?;";
    private static final String SEARCH_BY_ID = "select * from lms.book where id_book = ?;";
    private static final String SEARCH_BOOK = "select * from lms.book where ";
    private static final String SEARCH_BOOK2 = "select *from lms.book where title like ? "
            + "or pc like ? or author like ?;";
    private final String SEARCH_QUANTITY = "select * from lms.bookquantity where id_book = ?";
    private static final String FIND_PC = "select distinct(pc) from book ";
    private static final String FIND_CLS = "select distinct(classification) from book ";

    /**
     * Khởi tạo đối tượng Book với đầy đủ tham số.
     *
     * @param id đối số xác định id của đối tượng Book.
     * @param title đối số xác định title của đối tượng Book.
     * @param publishingCompany đối số xác định publishingCompanyn của đối tượng
     * Book.
     * @param author đối số xác định author của đối tượng Book.
     * @param classification đối số xác định classification của đối tượng Book.
     */
    public Book(int id, String title, String publishingCompany, String author, String classification) {
        this.id = id;
        this.title = title;
        this.publishingCompany = publishingCompany;
        this.author = author;
        this.classification = classification;
    }

    /**
     * Khởi tạo đối tượng Book
     */
    public Book() {

    }

    /**
     * Đặt giá trị cho thuộc tính title của đối tượng Book
     *
     * @param title đối số xác định giá trị của title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Trả về thuộc tính tên của đối tượng Book.
     *
     * @return Chuỗi kí tự thuộc tính tên của đối tượng Book.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Đặt giá trị cho thuộc tính publishingCompany của đối tượng Book
     *
     * @param publishingCompany đối số xác định giá trị của publishingCompany.
     */
    public void setPublishingCompany(String publishingCompany) {
        this.publishingCompany = publishingCompany;
    }

    /**
     * Trả về giá trị của thuộc tính publishingCompany của đối tượng Book
     *
     * @return giá trị của thuộc tính publishingCompany.
     */
    public String getPublishingCompany() {
        return this.publishingCompany;
    }

    /**
     * Đặt giá trị cho thuộc tính author của đối tượng Book
     *
     * @param author đối số xác định giá trị của author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Trả về giá trị của thuộc tính author của đối tượng Book
     *
     * @return giá trị của thuộc tính author.
     */
    public String getAuthor() {
        return this.author;
    }

    /**
     * Đặt giá trị cho thuộc tính id của đối tượng Book
     *
     * @param number đối số xác định giá trị của id
     */
    public void setId(int number) {
        this.id = number;
    }

    /**
     * Trả về giá trị của thuộc tính id của đối tượng Book
     *
     * @return giá trị của thuộc tính id.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Đặt giá trị cho thuộc tính classification của đối tượng Book
     *
     * @param clasification đối số xác định giá trị của classification
     */
    public void setClassification(String classification) {
        this.classification = classification;
    }

    /**
     * Trả về giá trị của thuộc tính classification của đối tượng Book
     *
     * @return giá trị của thuộc tính classification.
     */
    public String getClassification() {
        return this.classification;
    }

    /**
     * Tìm kiếm sách theo các tham số truyền vào
     *
     * @param classification đối số xác định thể loại của sách.
     * @param title đối số xác định title của sách
     * @param pc đối số xác định nhà xuất bản của sách
     * @param author đối số xác định tác giả của sách.
     * @return đối tượng Book có thông tin giống thông tin truyền vào.
     * @throws SQLException
     */
    public static ArrayList<Book> searchBook(String classification, String title, String pc, String author) throws SQLException {
        ArrayList<Book> books = new ArrayList<Book>();
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        String sql = SEARCH_BOOK;
        String queryForClassification = "classification like ?";
        String queryForTitle = "title like ?";
        String queryForPC = "pc like ?";
        String queryForAuthor = "author like ?";
        int count = 0;
        if ((classification != null)) {
            sql += queryForClassification;
            count++;
        }
        if ((title != null)) {
            if (count != 0) {
                sql += " and ";
            }
            sql += queryForTitle;
            count++;
        }
        if ((pc != null)) {
            if (count != 0) {
                sql += " and ";
            }
            sql += queryForPC;
            count++;
        }
        if ((author != null)) {
            if (count != 0) {
                sql += " and ";
            }
            sql += queryForAuthor;
        }
        count = 1;
        PreparedStatement ptmt = connection.prepareStatement(sql);
        if ((classification != null)) {
            ptmt.setString(count, "%" + classification + "%");
            count++;
        }
        if ((title != null)) {
            ptmt.setString(count, "%" + title + "%");
            count++;
        }
        if ((pc != null)) {
            ptmt.setString(count, "%" + pc + "%");
            count++;
        }
        if ((author != null)) {
            ptmt.setString(count, "%" + author + "%");

        }
        ResultSet rs = ptmt.executeQuery();
        while (rs.next()) {
            Book book = new Book();
            book.setId(rs.getInt("id_book"));
            book.setClassification(rs.getString("classification"));
            book.setTitle(rs.getString("title"));
            book.setPublishingCompany(rs.getString("pc"));
            book.setAuthor(rs.getString("author"));
            books.add(book);
        }
        connection.close();
        return books;
    }

    /**
     * Tìm kiếm sách thông qua tiêu đề
     *
     * @param info đối số xác định tiêu đề sách
     * @return ArrayList<Book> các Book có thông tin giống tham số truyền vào.
     * @throws SQLException
     */
    public static ArrayList<Book> searchBook(String info) throws SQLException {
        ArrayList<Book> books = new ArrayList<Book>();
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(SEARCH_BOOK2);
        ptmt.setString(1, info);
        ptmt.setString(2, info);
        ptmt.setString(3, info);
        ResultSet rs = ptmt.executeQuery();
        while (rs.next()) {
            Book book = new Book();
            book.setId(rs.getInt("id_book"));
            book.setTitle(rs.getString("title"));
            book.setPublishingCompany(rs.getString("pc"));
            book.setAuthor(rs.getString("author"));
            book.setClassification(rs.getString("classification"));

            books.add(book);
        }
        connection.close();
        return books;
    }

    /**
     * Tìm kiếm sách bằng tiêu đề
     *
     * @param title đối số xác định tiêu đề sách.
     * @return <li>true nếu tìm thấy sách</li>
     * <li>false nếu không tìm thấy sách</li>
     * @throws SQLException
     */
    public boolean findByTitle(String title) throws SQLException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(SEARCH_BY_TITLE);
        ptmt.setString(1, title);
        ResultSet rs = ptmt.executeQuery();
        if (rs.next()) {
            this.setId(rs.getInt("id_book"));
            this.setTitle(rs.getString("title"));
            this.setAuthor(rs.getString("author"));
            this.setPublishingCompany(rs.getString("pc"));
            this.setClassification(rs.getString("classification"));
            connection.close();
            return true;
        }
        connection.close();
        return false;

    }

    /**
     * Tìm kiếm sách bằng id sách
     *
     * @param id đối số xác định id
     * @return đối tượng Book có thông tin giống thông tin truyền vào
     * @throws SQLException
     */
    public static Book findByID(int id) throws SQLException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(SEARCH_BY_ID);
        ptmt.setInt(1, id);
        ResultSet rs = ptmt.executeQuery();
        Book book = new Book();
        if (rs.next()) {
            book.setId(rs.getInt("id_book"));
            book.setTitle(rs.getString("title"));
            book.setAuthor(rs.getString("author"));
            book.setPublishingCompany(rs.getString("pc"));
            book.setClassification(rs.getString("classification"));
            connection.close();
            return book;
        }
        connection.close();
        return null;

    }

    /**
     * Lấy danh sách các nhà xuất bản của Sách
     *
     * @return Danh sách các nhà xuất bản kiểu String
     * @throws SQLException
     */
    public static ArrayList<String> findPC() throws SQLException {
        ArrayList<String> pcs = new ArrayList<String>();
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(FIND_PC);
        ResultSet rs = ptmt.executeQuery();
        while (rs.next()) {
            String pc;
            pc = rs.getString("pc");
            pcs.add(pc);
        }
        return pcs;
    }

    /**
     * Lấy danh sách các thể loại của sách
     *
     * @return Danh sách các thể lạo của sách.
     * @throws SQLException
     */
    public static ArrayList<String> findCLS() throws SQLException {
        ArrayList<String> clss = new ArrayList<String>();
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(FIND_CLS);
        ResultSet rs = ptmt.executeQuery();
        while (rs.next()) {
            String cls;
            cls = rs.getString("classification");
            clss.add(cls);
        }
        return clss;
    }

    /**
     * So sánh hai đối tượng Book
     *
     * @param obj Đối tượng Book truyền vào,
     * @return <li>true nếu hai đối tương Booklà giống nhau</li>
     * <li>false nếu hai đối tượng Book là khác nhau</li>
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Book) {
            Book book2 = ((Book) obj);
            return (this.id == book2.getId())
                    && (this.title.equals(book2.getTitle()))
                    && (this.author.equals(book2.getAuthor()))
                    && (this.publishingCompany.equals(book2.getPublishingCompany()))
                    && (this.classification.equals(book2.getClassification()));
        } else {
            return false;
        }
    }

}
