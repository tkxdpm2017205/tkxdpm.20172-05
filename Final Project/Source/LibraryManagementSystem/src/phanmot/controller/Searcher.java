/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import java.util.ArrayList;
import phanmot.entity.Book;

/**
 *
 * @author username
 */
public interface Searcher {

    /**
     * Trả về đối tượng sách có liên quan đến các tham số truyền vào.
     *
     * @param str1 đối số xác định thể loại của đối tượng Book được trả về.
     * @param str2 đối số xác định tên của đối tượng Book được trả về.
     * @param str3 đối số xác định nhà xuất bản loại của đối tượng Book được trả về.
     * @param str4 đối số xác định tác giả của đối tượng Book được trả về.
     * @return Đối tượng Book có liên quan đến tham số truyền vào.
     */
    public ArrayList<Book> search(String str1, String str2, String str3, String str4);
}
