/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author username
 */
public class DBConnection {

    private Connection connection;
    private static final String URL = "jdbc:mysql://localhost:3306/lms?characterEncoding=UTF-8";
    private static final String USER = "root";
    private static final String PASSWORD = "phuong";

    public Connection connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (ClassNotFoundException ex) {
            System.out.println("Load drive khong thanh cong");
        } catch (SQLException ex) {
            System.out.println("Loi: " + ex.getMessage());
        }
        return connection;
    }
}
