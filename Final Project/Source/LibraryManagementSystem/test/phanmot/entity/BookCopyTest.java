/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author username
 */
public class BookCopyTest {

    BookCopy instance;

    public BookCopyTest() {
    }

    @Before
    public void setUp() {
        instance = new BookCopy();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of updateState method, of class BookCopy.
     */
    @Test
    public void testUpdateState() throws Exception {
        //Kiem tra update
        Book book = new Book(207, "Anh có thích nước Mỹ không?", "Văn Học", "Tân Di Ổ", "Ngôn tình");
        BookCopy instance = new BookCopy(book, 207001, "available");
        assertEquals(true, instance.updateState("rent"));

    }

    /**
     * Test of checkBookQuantity method, of class BookCopy.
     */
    @Test
    public void testCheckBookQuantity() throws Exception {
        //con sach.
        assertEquals(true, instance.checkBookQuantity(201));

        //het sach
        assertEquals(false, instance.checkBookQuantity(205));
    }

    /**
     * Test of findBook method, of class BookCopy.
     */
    @Test
    public void testFindBook() throws Exception {
        //tim dc sach
        instance.findBook(201);
        assertEquals(201003, instance.getIndex());

        //khong tim duoc sach        
        assertEquals(false, instance.findBook(205));
    }

    /**
     * Test of findBookCopy method, of class BookCopy.
     */
    @Test
    public void testFindBookCopyExistResult() throws Exception {
        int index = 207002;
        Book book = new Book(207, "Anh có thích nước Mỹ không?", "Văn Học", "Tân Di Ổ", "Ngôn tình");
        BookCopy expResult = new BookCopy(book, 207002, "available");
        BookCopy result = BookCopy.findBookCopy(index);
        System.out.println(expResult.equals(result));
        assertEquals(expResult, result);
    }

    @Test
    public void testFindBookCopyNullResult() throws Exception {

        assertNull(BookCopy.findBookCopy(207003));
    }

}
