/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import java.util.ArrayList;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import phanmot.entity.Book;

/**
 *
 * @author username
 */
public class BookSearcherTest {
    
    public BookSearcher instance;
   
    @Before
    public void setUp() {
        //Thực hiện các đoạn mã khởi tạo một lần ở đây  
        instance = new BookSearcher();
    }  
    @After
    public void tearDown(){
        
    }
    /**
     * Test of search method, of class BookSearcher.
     */
    @Test
    public void testSearchInputNull() {       
        //Dau vao trong.
        ArrayList<Book> result = instance.search(null, null, null, null);
        assertNull(result);                               
        
    }
    @Test
    public void testSearchNullResult(){
        //Khong tim thay sach.
        ArrayList<Book> result = instance.search(null, "310", null, null);
        assertNull(result);
        
    }
    @Test
    public void testSearchRExistResult(){
        //Tim thay sách.        
        String str1 = "A Game of Thrones";
        Book expResult = new Book(202, "A Game of Thrones", "Trẻ", "George R.R Martin", "Giả tưởng");
        ArrayList<Book> result = instance.search(null, str1, null, null);
        assertEquals("Loi tim kiem sai.",expResult,result.get(0));  
    }
    
}
