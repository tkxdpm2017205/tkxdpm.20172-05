/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import phanmot.entity.Book;

/**
 *
 * @author username
 */
public class CancelRegistrationControllerTest {
    CancelRegistrationController instance;
    public CancelRegistrationControllerTest() {
    }
    
    @Before
    public void setUp() {
        instance = new CancelRegistrationController();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of cancelConfirmationRequest method, of class CancelRegistrationController.
     */
    @Test
    public void testCancelConfirmationRequest() throws Exception {
        //xac nhan khong xoa
        boolean result = instance.cancelConfirmationRequest(1507, Book.findByID(207), false);
        assertEquals(false, result);
        
        //xac nhan xoa
        result = instance.cancelConfirmationRequest(1507, Book.findByID(207), true);
        assertEquals(true, result);
    }
    
}
