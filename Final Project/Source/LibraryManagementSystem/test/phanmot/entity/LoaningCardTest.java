/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.entity;

import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author username
 */
public class LoaningCardTest {

    public LoaningCardTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of cardCheck method, of class LoaningCard.
     */
    @Test
    public void testCardCheck() {
        //Qua han
        LoaningCard instance = new LoaningCard(1, new Date(118, 04, 1), new Date(118, 04, 9));
        assertEquals(false, instance.cardCheck());

        //Dung ngay
        instance = new LoaningCard(2, new Date(118, 04, 1), new Date());
        assertEquals(true, instance.cardCheck());

        //Chua het han
        instance = new LoaningCard(3, new Date(118, 04, 1), new Date(118, 11, 31));
        assertEquals(true, instance.cardCheck());
    }

    /**
     * Test of findCard method, of class LoaningCard.
     */
    @Test
    public void testFindCardExistResult() throws Exception { 
        int id = 1;
        LoaningCard expResult = new LoaningCard(1, new Date(118, 04, 5), new Date(118, 04, 9));
        LoaningCard result = LoaningCard.findCard(id);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testFindCardNullResult() throws Exception {
        int id = 4;
        LoaningCard result = LoaningCard.findCard(id);
        assertNull(result);

    }

}
