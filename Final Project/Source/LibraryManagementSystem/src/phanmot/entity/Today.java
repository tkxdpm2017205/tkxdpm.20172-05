/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author username
 */
public class Today {

    private Date date;
    private static Today today;

    private Today() {
        date = new Date();
    }

    public static Today getInstance() {
        if (today == null) {
            return new Today();
        }
        return today;
    }

    /**
     * Lấy thời gian hôm nay theo String
     *
     * @return Thời gian hôm nay dạng String yyyy-mm-dd
     */
    private String getStringToday() {
        String year = String.valueOf(date.getYear() + 1900);
        String month = String.valueOf(date.getMonth() + 1);
        String day = String.valueOf(date.getDate());
        String con = year + "-" + month + "-" + day;
        return con;
    }

    /**
     * Lấy thời gian hôm nay theo kiểu của java.sql.Date.
     *
     * @return Thời gian hôm nay dạng String yyyy-mm-dd theo kiểu của
     * java.sql.Date.
     * @throws ParseException
     */
    public java.sql.Date getToday() throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = formatter.parse(getStringToday());
        java.sql.Date sqlDate = new java.sql.Date(myDate.getTime());
        return sqlDate;
    }

}
