/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import java.util.ArrayList;
import phanmot.entity.Book;

/**
 *
 * @author username
 */
public class BookSearchingController {

    /**
     * Trả về đối tượng Book có thông tin như các tham số truyền vào
     *
     * @param str1 đối số xác định thể loại của đối tượng Book muốn tìm.
     * @param str2 đối số xác định tên của đối tượng Book muốn tìm.
     * @param str3 đối số xác định nhà xuất bản của đối tượng Book muốn tìm.
     * @param str4 đối số xác định tác giả của đối tượng Book muốn tìm.
     * @param search đối số xác định cách thức tìm kiếm đối tượng Book
     * @return Một ArrayLít<Book> có thông tin như tham số truyền vào.
     */
    public ArrayList<Book> search(String str1, String str2, String str3, String str4, Searcher search) {
        ArrayList<Book> books = search.search(str1, str2, str3, str4);
        return books;
    }
;
}
