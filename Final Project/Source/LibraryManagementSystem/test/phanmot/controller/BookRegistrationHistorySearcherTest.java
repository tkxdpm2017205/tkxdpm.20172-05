/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import phanmot.entity.Book;
import phanmot.entity.BookRegistrationHistory;

/**
 *
 * @author username
 */
public class BookRegistrationHistorySearcherTest {
    BookRegistrationHistorySearcher instance;
    public BookRegistrationHistorySearcherTest() {
    }
    
    @Before
    public void setUp() {
        instance = new BookRegistrationHistorySearcher();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of search method, of class BookRegistrationHistorySearcher.
     */
    @Test
    public void testSearchNullInput() {
        int str1 = 0;
        ArrayList<BookRegistrationHistory> result = instance.search(str1);
        assertNull(result);
    }
    @Test
    public void testSearchNullResult(){
        int int1 = 1789;
        ArrayList<BookRegistrationHistory> result = instance.search(int1);
        assertNull(result);
    }
    @Test
    public void testSearchExistResult(){
    }
    
}
