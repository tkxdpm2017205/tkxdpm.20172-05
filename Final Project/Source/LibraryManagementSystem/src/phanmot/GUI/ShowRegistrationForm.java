/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.GUI;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import phanmot.controller.BookRegistrationHistorySearcher;
import phanmot.entity.BookRegistrationHistory;

/**
 *
 * @author username
 */
public class ShowRegistrationForm extends javax.swing.JFrame {

    /**
     * Creates new form ShowRegistrationForm
     */
    public ShowRegistrationForm() {
        initComponents();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setModel() {
        this.showList();
    }
    
    private void showList(){
        this.list = new DefaultListModel();
        this.rentList = new DefaultListModel();
        BookRegistrationHistorySearcher searcher = new BookRegistrationHistorySearcher();
        this.histories = searcher.search(id);
        if (this.histories == null) {
            JOptionPane.showMessageDialog(null, "Chưa có lịch sử đăng kí mượn sách");

        } else {
            for (int i = 0; i < histories.size(); i++) {
                if (histories.get(i).getBookCopy().getState().equals("borrowed")) {
                    list.addElement(histories.get(i).getBookCopy().getBook().getTitle());
                } else if (histories.get(i).getBookCopy().getState().equals("rent")) {
                    rentList.addElement(histories.get(i).getBookCopy().getBook().getTitle());
                }
            }
            this.borrowedJList.setModel(list);
            this.rentJList.setModel(rentList);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bigPanel = new javax.swing.JPanel();
        titleLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        borrowedJList = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        rentJList = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Danh sách mượn sách");

        bigPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        titleLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        titleLabel.setText("Danh sách Đăng ký mượn sách");

        jScrollPane1.setViewportView(borrowedJList);

        jScrollPane2.setViewportView(rentJList);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Danh sách đăng kí mượn sách");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Danh sách đầu sách đã mượn ");

        javax.swing.GroupLayout bigPanelLayout = new javax.swing.GroupLayout(bigPanel);
        bigPanel.setLayout(bigPanelLayout);
        bigPanelLayout.setHorizontalGroup(
            bigPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bigPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bigPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bigPanelLayout.createSequentialGroup()
                        .addGroup(bigPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(bigPanelLayout.createSequentialGroup()
                                .addGroup(bigPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(titleLabel)
                                    .addComponent(jLabel2))
                                .addGap(0, 350, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(bigPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        bigPanelLayout.setVerticalGroup(
            bigPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bigPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addGap(23, 23, 23)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bigPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bigPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ShowRegistrationForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ShowRegistrationForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ShowRegistrationForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ShowRegistrationForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ShowRegistrationForm().setVisible(true);
            }
        });
    }

    private ArrayList<BookRegistrationHistory> histories;
    private DefaultListModel list;
    private DefaultListModel rentList;
    private int id;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bigPanel;
    private javax.swing.JList<String> borrowedJList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<String> rentJList;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
}
