/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import phanmot.database.DBConnection;
import phanmot.entity.Book;

/**
 *
 * @author username
 */
public class BookSearcher implements Searcher {

    private Connection connection;
    private final String SQL = "select * from lms.book where title = ? ";

    /**
     * Trả về đối tượng Book có thông tin như các tham số truyền vào.
     *
     * @param str1 đối số xác định thể loại của đối tượng Book muốn tìm.
     * @param str2 đối số xác định tên của đối tượng Book muốn tìm.
     * @param str3 đối số xác định nhà xuất bản của đối tượng Book muốn tìm.
     * @param str4 đối số xác định tác giả của đối tượng Book muốn tìm.
     * @return Một ArrayLít<Book> có thông tin như tham số truyền vào.
     */
    @Override
    public ArrayList<Book> search(String str1, String str2, String str3, String str4) {
        ArrayList<Book> books = new ArrayList<Book>();
        if (str1 == null && str2 == null && str3 == null && str4 == null) {
            return null;
        } else {
            try {
                books = Book.searchBook(str1, str2, str3, str4);
                if (books.size() == 0) {
                    return null;
                }
            } catch (SQLException ex) {
                Logger.getLogger(BookSearcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return books;
    }

}
