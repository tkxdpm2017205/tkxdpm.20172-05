/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import phanmot.database.DBConnection;

/**
 *
 * @author username
 */
public class Account {

    private int id;
    private String name;
    private String accountName;
    private String password;
    private LoaningCard loaningCard;
    private String email;
    private int phoneNumber;
    private String gender;
    private String role;
    private boolean status;
    private boolean requestToChange;

    private final String FIND_BY_USERNAME = "select * from lms.account where username = ?;";
    private static final String FIND_BY_ID = "select * from lms.account where id_account = ?;";
    private final String LOGIN = "select * from lms.account where username = ? and pass = ?";
    private final String UPDATE_PASSWORD = "update account set pass = ?, requesttochange = ? where username = ?; ";
    private final String ADD_USER = "insert into lms.account values (?,?,?,?,?,?,?,?,?,?,?);";

    /**
     * Khởi tạo đối tượng Account với đầy đủ tham số.
     *
     * @param id tham số xác định id của tài khoản.
     * @param name tham số xác đinh tên của tài khoản.
     * @param accountName tham số xác đinh username của tài khoản.
     * @param password tham số xác định mật khẩu của tài khoản.
     * @param loaningCard tham số xác định thẻ mượn sách của tài khoản.
     * @param email tham số xác định email của tài khoản.
     * @param phoneNumber tham số xác định sdt của tài khoản.
     * @param gender tham số xác định giới tính của tài khoản.
     * @param role tham số xác định vai trò của tài khoản.
     * @param status tham số xác định trạng thái tài khoản.
     * @param request tham số xác định yêu cầu thay đôi mật khẩu.
     */
    public Account(int id, String name, String accountName, String password,
            LoaningCard loaningCard, String email, int phoneNumber, String gender,
            String role, boolean status, boolean request) {
        this.id = id;
        this.name = name;
        this.accountName = accountName;
        this.password = password;
        this.loaningCard = loaningCard;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.role = role;
        this.status = status;
        this.requestToChange = request;
    }

    /**
     * Khởi tạo đối tượng Account
     */
    public Account() {

    }

    /**
     * Đặt giá trị cho thuộc tính id của đối tượng Account.
     *
     * @param id đối số xác định giá trị cần đặt cho id của đối tượng Account.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Trả về giá trị thuộc tính id của đối tượng Account.
     *
     * @return giá trị của thuộc tính id của đối tượng Account.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Đặt giá trị cho thuộc tính name của đối tượng Account.
     *
     * @param name đối số xác định giá trị cần đặt cho name của đối tượng
     * Account
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Trả về giá trị thuộc tính name của đối tượng Account.
     *
     * @return giá trị của thuộc tính name của đối tượng Account.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Đặt giá trị cho thuộc tính accountName của đối tượng Account.
     *
     * @param accountName giá trị của thuộc tính accountName của đối tượng
     * Account
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * Trả về giá trị thuộc tính accountName của đối tượng Account.
     *
     * @return giá trị của thuộc tính accountName của đối tượng Account.
     */
    public String getAcountName() {
        return this.accountName;
    }

    /**
     * Đặt giá trị cho thuộc tính password của đối tượng Account.
     *
     * @param password giá trị của thuộc tính password của đối tượng Account
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Trả về giá trị thuộc tính password của đối tượng Account.
     *
     * @return giá trị của thuộc tính password của đối tượng Account.
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Đặt giá trị cho thuộc tính loaningCard của đối tượng Account.
     *
     * @param loaningCard giá trị của thuộc tính loaningCard của đối tượng
     * Account
     */
    public void setLoaningCard(LoaningCard loaningCard) {
        this.loaningCard = loaningCard;
    }

    /**
     * Trả về giá trị thuộc tính loaningCard của đối tượng Account.
     *
     * @return giá trị của thuộc tính loaningCard của đối tượng Account.
     */
    public LoaningCard getLoaningCard() {
        return this.loaningCard;
    }

    /**
     * Đặt giá trị cho thuộc tính rolecủa đối tượng Account.
     *
     * @param role giá trị của thuộc tính role của đối tượng Account
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * Trả về giá trị thuộc tính role của đối tượng Account.
     *
     * @return giá trị của thuộc tính role của đối tượng Account.
     */
    public String getRole() {
        return this.role;
    }

    /**
     * Đặt giá trị cho thuộc tính status của đối tượng Account.
     *
     * @param status giá trị của thuộc tính status của đối tượng Account
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * Trả về giá trị thuộc tính status của đối tượng Account.
     *
     * @return giá trị của thuộc tính status của đối tượng Account.
     */
    public boolean getStatus() {
        return this.status;
    }

    /**
     * Đặt giá trị cho thuộc tính request của đối tượng Account.
     *
     * @param request giá trị của thuộc tính request của đối tượng Account
     */
    public void setRequestToChange(boolean request) {
        this.requestToChange = request;
    }

    /**
     * Trả về giá trị thuộc tính request của đối tượng Account.
     *
     * @return giá trị của thuộc tính request của đối tượng Account.
     */
    public boolean getRequestToChange() {
        return this.requestToChange;
    }

    /**
     * Đặt giá trị cho thuộc tính email của đối tượng Account.
     *
     * @param email giá trị của thuộc tính email của đối tượng Account
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Trả về giá trị thuộc tính email của đối tượng Account.
     *
     * @return giá trị của thuộc tính email của đối tượng Account.
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Đặt giá trị cho thuộc tính phoneNumber của đối tượng Account.
     *
     * @param phoneNumber giá trị của thuộc tính phoneNumber của đối tượng
     * Account
     */
    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Trả về giá trị thuộc tính phoneNumber của đối tượng Account.
     *
     * @return giá trị của thuộc tính phoneNumber của đối tượng Account.
     */
    public int getPhoneNumber() {
        return this.phoneNumber;
    }

    /**
     * Đặt giá trị cho thuộc tính gender của đối tượng Account.
     *
     * @param gender giá trị của thuộc tính gender của đối tượng Account
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * Trả về giá trị thuộc tính gender của đối tượng Account.
     *
     * @return giá trị của thuộc tính gender của đối tượng Account.
     */
    public String getGender() {
        return this.gender;
    }

    /**
     * Kiểm tra trạng thái đăng nhập
     *
     * @param username đối số xác định tên tài khoản.
     * @param password đối số xác định mật khẩu.
     * @return <li>0 nếu tài khoản bị khóa</li>
     * <li>1 nếu bị yêu cầu đổi tài khoản</li>
     * <li>2 nếu sai tài khoản</li>
     * <li>3 nếu sai mật khẩu</li>
     * @throws SQLException
     */
    public int checkLogin(String username, String password) throws SQLException {
        if (findByUsername(username)) {
            Connection connection;
            DBConnection db = new DBConnection();
            connection = db.connect();
            PreparedStatement ptmt = connection.prepareStatement(LOGIN);
            ptmt.setString(1, username);
            ptmt.setString(2, password);
            ResultSet rs = ptmt.executeQuery();
            if (rs.next()) {
                if (this.getStatus()) {
                    connection.close();
                    //Tai khoan bi khoa.
                    return 0;
                } else if (this.getRequestToChange()) {
                    connection.close();
                    //Yeu cau doi tai khoan;
                    return 1;
                } else {
                    connection.close();
                    return 2;
                }
            } else {
                connection.close();
                return 3;
            }

        }
        //Tai khoan khong ton tai.
        return 4;
    }

    /**
     * Tìm kiếm đối tượng Account thông qua username.
     *
     * @param username đối số xác định username của tài khoản.
     * @return <li>true nếu tìm được tài khoản</li>
     * <li>nếu không tìm được tài khoản</li>
     * @throws SQLException
     */
    public boolean findByUsername(String username) throws SQLException {
        Connection connection;
        DBConnection db = new DBConnection();
        connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(FIND_BY_USERNAME);
        ptmt.setString(1, username);
        ResultSet rs = ptmt.executeQuery();
        if (rs.next()) {
            this.setId(rs.getInt("id_account"));
            this.setName(rs.getString("realname"));
            this.setAccountName(rs.getString("username"));
            this.setPassword(rs.getString("pass"));
            this.setRole(rs.getString("role"));
            this.setEmail(rs.getString("email"));
            this.setLoaningCard(LoaningCard.findCard(rs.getInt("id_card")));
            this.setPhoneNumber(rs.getInt("phonenumber"));
            this.setGender(rs.getString("gender"));
            this.setStatus(rs.getBoolean("status"));
            this.setRequestToChange(rs.getBoolean("requesttochange"));
            connection.close();
            return true;
        }
        connection.close();
        return false;
    }

    /**
     * Kiểm tra tồn tại của đối tượng Account thông qua id.
     *
     * @param id đối số xác định id
     * @return True nếu tồn tại. False nếu không tồn tại.
     * @throws SQLException
     */
    public boolean checkID(int id) throws SQLException {
        Connection connection;
        DBConnection db = new DBConnection();
        connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(FIND_BY_ID);
        ptmt.setInt(1, id);
        ResultSet rs = ptmt.executeQuery();
        if (rs.next()) {
            this.setId(rs.getInt("id_account"));
            this.setName(rs.getString("realname"));
            this.setAccountName(rs.getString("username"));
            this.setPassword(rs.getString("pass"));
            this.setRole(rs.getString("role"));
            this.setEmail(rs.getString("email"));
            this.setLoaningCard(LoaningCard.findCard(rs.getInt("id_card")));
            this.setPhoneNumber(rs.getInt("phonenumber"));
            this.setGender(rs.getString("gender"));
            this.setStatus(rs.getBoolean("status"));
            this.setRequestToChange(rs.getBoolean("requesttochange"));
            connection.close();
            return true;
        }
        connection.close();
        return false;
    }

    /**
     * Tìm kiếm đối tượng Account thông qua id.
     *
     * @param id đối số xác định id
     * @return Đối tượng Account có id như giá trị truyền vào.
     * @throws SQLException
     */
    public static Account finByID(int id) throws SQLException {
        Connection connection;
        DBConnection db = new DBConnection();
        connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(FIND_BY_ID);
        ptmt.setInt(1, id);
        ResultSet rs = ptmt.executeQuery();
        Account account = new Account();
        if (rs.next()) {
            account.setId(rs.getInt("id_account"));
            account.setName(rs.getString("realname"));
            account.setAccountName(rs.getString("username"));
            account.setPassword(rs.getString("pass"));
            account.setRole(rs.getString("role"));
            account.setEmail(rs.getString("email"));
            account.setLoaningCard(LoaningCard.findCard(rs.getInt("id_card")));
            account.setPhoneNumber(rs.getInt("phonenumber"));
            account.setGender(rs.getString("gender"));
            account.setStatus(rs.getBoolean("status"));
            account.setRequestToChange(rs.getBoolean("requesttochange"));
            connection.close();
            return account;
        }
        connection.close();
        return account;
    }
    
    
    /**
     * Cập nhật password
     *
     * @param username đối số xác định username cần đối mật khẩu.
     * @param password đối số xác đinh mật khẩu mới.
     * @throws SQLException
     */
    public void updatePassword(String username, String password) throws SQLException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(UPDATE_PASSWORD);
        ptmt.setString(1, password);
        ptmt.setInt(2, 0);
        ptmt.setString(3, username);
        ptmt.executeUpdate();
        connection.close();

    }

    /**
     * Thêm tài khoản mới
     *
     * @return <li>true nếu thêm thành công</li>
     * <li>false nếu không thành công</li>
     * @throws SQLException
     */
    public boolean addUser() throws SQLException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(FIND_BY_USERNAME);
        ptmt.setString(1, this.accountName);
        ResultSet rs = ptmt.executeQuery();
        if (rs.next()) {
            connection.close();
            return false;
        } else {
            ptmt = connection.prepareStatement(ADD_USER);
            ptmt.setInt(1, id);
            ptmt.setString(2, name);
            ptmt.setString(3, accountName);
            ptmt.setString(4, password);
            ptmt.setString(5, role);
            ptmt.setInt(6, loaningCard.getCardNumber());
            ptmt.setBoolean(7, status);
            ptmt.setBoolean(8, requestToChange);
            ptmt.setString(9, email);
            ptmt.setInt(11, phoneNumber);
            ptmt.setString(10, gender);
            ptmt.executeUpdate();
        }
        connection.close();
        return true;
    }
}
