/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import java.sql.SQLException;
import phanmot.entity.Account;

/**
 *
 * @author username
 */
public class AccountController {

    Account account = new Account();

    private static AccountController me;

    private AccountController() {

    }

    public static AccountController getInstance() {
        if (me == null) {
            me = new AccountController();
        }
        return me;
    }

    public int checkLogin(String email, String password) throws SQLException {
        return account.checkLogin(email, password);
    }

    public boolean register(Account account) throws SQLException {
        return account.addUser();
    }

    /**
     * Kiểm tra hợp lệ của đối tượng Account có tên trừng với tham số str1
     * truyền vào.
     * <li>Trả về giá trị True nếu tài khoản hợp lệ.
     * <li>Trả về giá trị False nếu tài khoản không hợp lệ.
     *
     * @param str1 đối số xác định tên đối tượng Account được kiểm tra.
     * @return Trả về True nếu tài khoản hợp lệ, các trường hợp khác trả về
     * false.
     */
    public boolean checkAccount(String str1) {
        if (str1 == null) {
            return false;
        }
        return true;
    }
;
}
