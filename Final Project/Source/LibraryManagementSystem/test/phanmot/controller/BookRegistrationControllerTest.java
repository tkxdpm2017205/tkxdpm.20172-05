/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import phanmot.entity.Account;
import phanmot.entity.Book;
import phanmot.entity.BookRegistrationHistory;
import phanmot.entity.LoaningCard;

/**
 *
 * @author username
 */
public class BookRegistrationControllerTest {

    BookRegistrationController instance;

    public BookRegistrationControllerTest() {
    }

    @Before
    public void setUp() {
        instance = new BookRegistrationController();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of checkRegistration method, of class BookRegistrationController.
     */
    @Test
    public void testCheckRegistration() throws Exception {
        //Muon qua nhieu.
        int result = instance.checkRegistration(1501, Book.findByID(207), 1);
        assertEquals(1, result);

        //The het han
        result = instance.checkRegistration(1505, Book.findByID(207), 1);
        assertEquals(3, result);

        //Het copy
        result = instance.checkRegistration(1507, Book.findByID(205), 1);
        assertEquals(2, result);

        //Thanh cong
        result = result = instance.checkRegistration(1507, Book.findByID(207), 1);
        assertEquals(0, result);

    }


}
