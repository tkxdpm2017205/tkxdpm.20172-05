/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework01;

import java.util.Date;

/**
 *
 * @author username
 */
public class DateUtil {
    private static final int[] MONTH_01 = {1,3,5,7,8,10,12};
    private static final int[] MONTH_02 = {4,6,9,11};

    public static boolean checkDate(int day, int month, int year){
        for(int i = 0; i < MONTH_01.length; i++)
            if(month == MONTH_01[i])
                if(day <=31 && day >0)
                    return true;
        for(int j = 0; j < MONTH_02.length; j++)
            if(month == MONTH_02[j])
                if(day <=30 && day >0)
                    return true;
        if(month == 2)
            if(year%4 != 0){
               if(day <=28 && day >0)
                    return true; 
            }                
            else{
                if(day <=29 && day >0)
                    return true;
            }                
            
        return false;
    }
    
    public static int calculateAge(int year){
        Date today = new Date(System.currentTimeMillis());
        int realYear = today.getYear() + 1900;
        int age;
        age = realYear - year;
        if (age >= 0)
            return age;
        else 
            return 0;
       
    }
}
