/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import phanmot.database.DBConnection;

/**
 *
 * @author username
 */
public class BookRegistrationHistory {

    private Account account;
    private BookCopy copy;
    private Date dayborrow;
    private Date dayrent;

    private static int LIMIT = 5;
    private static final String SEARCH_BY_ID = "select * from lms.bookhistory where id_account = ?;";
    private static final String COUNT_BOOK = "select count(bookindex) from lms.bookhistory where id_account = ?";
    private final String UPDATE = "insert into lms.bookhistory values (?,?,?,?)";
    private final String SEARCH_HISTORY = "select * from lms.bookhistory where id_account = ? and bookindex = ?";
    private final String DELETE = "delete from lms.bookhistory where id_account = ? and bookindex = ?";

    public BookRegistrationHistory(Account account, BookCopy copy) {
        this.account = account;
        this.copy = copy;
    }

    public BookRegistrationHistory() {

    }

    /**
     * Đặt giá trị cho thuộc tính copy của đối tượng BookRegstrationHistory.
     *
     * @param copy đối số xác định giá trị của tham số copy.
     */
    public void setBookCopy(BookCopy copy) {
        this.copy = copy;
    }

    /**
     * Trả về danh sách sách đã mượn của tài khoản.
     *
     * @return Trả về một danh sách các đối tượng Book liên quan đến Account
     */
    public BookCopy getBookCopy() {
        return this.copy;
    }

    /**
     * Đặt giá trị cho thuộc tính account của đối tượng BookRegstrationHistory.
     *
     * @param account đối số xác định giá trị của tham số account.
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * Trả về thuộc tính account.
     *
     * @return Trả về giá trị thuộc tính account
     */
    public Account getAccount() {
        return this.account;
    }

    /**
     * Đặt giá trị cho thuộc tính dayBorrow của đối tượng
     * BookRegstrationHistory.
     *
     * @param date đối số xác định giá trị của tham số dayBorrow.
     */
    public void setDayBorrow(Date date) {
        this.dayborrow = date;
    }

    /**
     * Trả về thuộc tính dayBorrow.
     *
     * @return Trả về giá trị thuộc tính dayBorrow
     */
    public Date getDayBorrow() {
        return this.dayborrow;
    }

    /**
     * Đặt giá trị cho thuộc tính copy của đối tượng BookRegstrationHistory.
     *
     * @param date đối số xác định giá trị của tham số dayrent.
     */
    public void setDayRent(Date date) {
        this.dayrent = date;
    }

    /**
     * Trả về thuộc tính dayRent.
     *
     * @return Trả về giá trị thuộc tính dayRent
     */
    public Date getDayRent(Date date) {
        return this.dayrent;
    }

    /**
     * Tìm kiếm các đối tượng BookRegistrationHistory của một tài khoản.
     *
     * @param id đối số xác định id của tài khoản.
     * @return Arraylist<BookRegistrationHistory> của tài khoản.
     * @throws SQLException
     */
    public static ArrayList<BookRegistrationHistory> search(int id) throws SQLException {
        ArrayList<BookRegistrationHistory> histories = new ArrayList<BookRegistrationHistory>();
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(SEARCH_BY_ID);
        ptmt.setInt(1, id);
        ResultSet rs = ptmt.executeQuery();
        int a = 0;
        while (rs.next()) {
            BookRegistrationHistory history = new BookRegistrationHistory();
            history.setAccount(Account.finByID(rs.getInt("id_account")));
            history.setBookCopy(BookCopy.findBookCopy(rs.getInt("bookindex")));
            history.setDayBorrow(rs.getDate("dayborrow"));
            history.setDayRent(rs.getDate("dayrent")); 

            histories.add(a, history);
        }
        connection.close();
        return histories;
    }

    /**
     * Kiểm tra số lượng sách đã mượn đã quá giới hạn hay chưa.
     * <li>Trả về <b>True</b> nếu chưa quá.
     * <li>Trả về <b>False</b> nếu vượt quá.
     *
     * @return Trả về <b>True</b> nếu chưa vượt quá giới hạn mượn sách, trả về
     * <b>False</b> nếu đã vượt quá.
     */
    public boolean check(int number) throws SQLException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(COUNT_BOOK);
        ptmt.setInt(1, this.getAccount().getId());
        ResultSet rs = ptmt.executeQuery();
        if (rs.next()) {
            int quantity = rs.getInt("count(bookindex)");
            if (number + quantity <= LIMIT) {
                connection.close();
                return true;
            } else {
                connection.close();
                return false;
            }
        }
        connection.close();
        return false;
    }

    /**
     * Thêm sách vào lịch sử mượn sách.
     *
     * @throws java.sql.SQLException
     */

    public boolean addBookRegisTrationHistory() throws SQLException, ParseException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(SEARCH_HISTORY);
        ptmt.setInt(1, this.getAccount().getId());
        ptmt.setInt(2, this.getBookCopy().getIndex());
        ResultSet rs = ptmt.executeQuery();
        if(rs.next()){
            connection.close();
            return false;
        }
        else{
            ptmt = connection.prepareStatement(UPDATE);
            ptmt.setInt(1, this.getAccount().getId());
            ptmt.setInt(2, this.getBookCopy().getIndex());           
            ptmt.setDate(3, Today.getInstance().getToday());
            ptmt.setInt(4, 19000100);
            ptmt.executeUpdate();
            connection.close();
            return true;
        }       


    }

    /**
     * Xóa lịch sử mượn sách.
     *
     * @throws java.sql.SQLException
     */
    public boolean removeBookRegisTrationHistory() throws SQLException {
        DBConnection db = new DBConnection();
        Connection connection = db.connect();
        PreparedStatement ptmt = connection.prepareStatement(SEARCH_HISTORY);
        ptmt.setInt(1, this.getAccount().getId());
        ptmt.setInt(2, this.getBookCopy().getIndex());
        ResultSet rs = ptmt.executeQuery();
        if (!rs.next()) {
            connection.close();
            return false;
        } else {
            ptmt = connection.prepareStatement(DELETE);
            ptmt.setInt(1, this.getAccount().getId());
            ptmt.setInt(2, this.getBookCopy().getIndex());
            ptmt.executeUpdate();
            connection.close();
            return true;
        }
    }

}
