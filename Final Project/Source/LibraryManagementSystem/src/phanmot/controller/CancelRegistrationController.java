/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import phanmot.entity.Account;
import phanmot.entity.Book;
import phanmot.entity.BookCopy;
import phanmot.entity.BookRegistrationHistory;

/**
 *
 * @author username
 */
public class CancelRegistrationController {

    /**
     * Xóa danh sách đăng ký mượn sách
     * @param id đối số xác định tài khoản muốn thực hiện xóa sách
     * @param book đôi số xác định sách muốn thực hiện xóa.
     * @return Trả về <li>0 nếu thực hiện thành công</li> 
     * <li>1 nếu không thể thực hiện việc thay đổi lại trạng thái sách</li> 
     * <li>2 nếu không thể thực hiện được việc xóa đăng ký</li> 
     * @throws java.sql.SQLException
     */
    private int cancelRequest(int id, Book book) throws SQLException {
        ArrayList<BookRegistrationHistory> histories = BookRegistrationHistory.search(id);
        for (BookRegistrationHistory history : histories) {
            if (history.getBookCopy().getBook().equals(book)) {
                if (history.removeBookRegisTrationHistory()) {
                    BookCopy copy = BookCopy.findBookCopy(history.getBookCopy().getIndex());
                    if (copy.updateState("available")) {
                        return 0; //thanh cong
                    }
                }
                return 1; // không thay doi dc trang thai copy;
            }
        }
        return 2; //khong xoa duoc dang ki muon sach
    }

    /**
     * Xác nhận đăng kí mượn sách
     * @param id đối số xác định tài khoản muốn thực hiện xóa sách
     * @param book đôi số xác định sách muốn thực hiện xóa.
     * @param confirmation đối số xác định việc xác nhận xóa.
     * @return Trả về <li>true nếu xác nhận đúng</li>
     * <li>false nếu xác nhận sai</li>
     * @throws java.sql.SQLException
     */
    public boolean cancelConfirmationRequest(int id, Book book, boolean confirmation) throws SQLException {
        if (confirmation) {
            if (cancelRequest(id, book) == 0) {
                return true;
            }
        }
        return false;
    }
}
