/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanmot.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import phanmot.entity.Book;
import phanmot.entity.BookRegistrationHistory;

/**
 *
 * @author username
 */
public class BookRegistrationHistorySearcher {

    /**
     * Trả về đối tượng <b>BookRegistrationHistory</b> mang thông tin về việc mượn sách
     * của đối tượng Account có id trùng với tham số str1 truyền vào.
     *
     * @param str1 đối số xác định tên của đối tượng Account cần xác định tham
     * số <b>BookRegistrationHistory</b>
     * @return Đối tượng <b>BookRegistrationHistory</b> mang thông tin về việc mượn
     * sách của đối tượng Account có tên trùng với tham số Str1 truyền vào.
     */
    public ArrayList<BookRegistrationHistory> search(Object str1) {
        int str2 = (int) str1;
        if (str2 == 0) {
            return null;
        } else {
            try {
                ArrayList<BookRegistrationHistory> histories = BookRegistrationHistory.search(str2);
                if (histories.size() == 0) {
                    return null;
                }
                return histories;
            } catch (SQLException ex) {
                Logger.getLogger(BookRegistrationHistorySearcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

}
